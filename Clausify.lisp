;==============================================================================
;                            MEMBRI GRUPPO PROGETTO
;==============================================================================
;
;    NOME/COG: Luca Samuele Vanzo (735924)
;    NOME/COG: Marco Piovani (735380)
;    NOME/COG: Matteo D'Aniello (735235)
;
;==============================================================================
;                       INFORMAZIONI/RICHIESTE PROGETTO
;==============================================================================
;
;    Linguaggi di Programmazione
;    Modulo di Laboratorio di Linguaggi di Programmazione 
;    Progetto Common-Lisp Febbraio 2013
;
;    Clausify (Formule ben Formate)
;    Marco Antoniotti, Gabriella Pasi e Pietro Braione
; 
;    - INTRODUZIONE -
;    L'obiettivo di questo progetto e' di costruire un programma 
;    Common Lisp in grado di trasformare formule ben formate 
;    (fbf ) di un linguaggio logico del primo ordine in forma 
;    normale congiunta (conjunctive normal form o cnf ).
;
;    Supponiamo di rapresentare le fbf nel modo seguente:
;    - termine      ::= <costante> | <variabile> | <funzione>
;    - costante     ::= <numeri> | <simboli che cominciano con lettera>
;    - variabile    ::= <simboli che cominciano con ?>
;    - funzione     ::= '(' <simbolo> <termine>* ')'
;
;    - fbf          ::= <predicato>
;       | <negazione> | <opand-fbf> | <opor-fbf> | <implicazione>
;       | <universale> | <esistenziale>
;
;    - predicato    ::= <simbolo che comincia con una lettera>
;       | '(' <simbolo> <termine>* ')'
;
;    - negazione    ::= '(' not <fbf> ')'
;    - opand-fbf         ::= '(' and <fbf> <fbf> ')'
;    - opor-fbf         ::= '(' or <fbf> <fbf> ')'
;    - implicazione ::= '(' ==> <fbf> <fbf> ')'
;    - universale   ::= '(' forall <variabile> <fbf> ')'
;    - esistenziale ::= '(' exist <variabile> <fbf> ')'
;
;    Naturalmente dovrete anche considerare fbf piu' complesse quali:
;    - opand-fbf'        ::= '(' and <fbf>* ')'
;    - opor-fbf'        ::= '(' or <fbf>* ')'
;
;==============================================================================
;                              MAPPATURA ERRORI
;==============================================================================

(defun spawn-str (err-id)
    (cond
        ;----------------------------------------------------------------------
        ((= err-id 0)                                       ; STRING ID:      0
            (error "Non e' una formula ben formata!")
        )
        ;----------------------------------------------------------------------
        (T nil)
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                        FUNZIONE FORNITE DAL PROGETTO
;==============================================================================
; Dato un certo input:
;   - genera delle variabili di skolem
;   - genera delle funzioni di skolem
;==============================================================================

(defun skolem-variable () 
    (gentemp "SV-")
)

;==============================================================================

(defun skolem-function* (&rest args) 
    (cons (gentemp "SF-") args)
)

;==============================================================================

(defun skolem-function (args) 
    (apply #'skolem-function* args)
)

;==============================================================================

(defun variablep (v)
    (and 
        (symbolp v) 
        (char= #\? (char (symbol-name v) 0))
    )
)

;==============================================================================
;                       FUNZIONE PRINCIPALE - FBF2CNF
;==============================================================================

(defun fbf2cnf (fbf)
    (if (check-fbf fbf)
        (create-formula 
            (defold fbf)
        )
        (spawn-str 0)
    )
)

;==============================================================================
;                       FUNZIONE DI CONTROLLO - HORNP
;==============================================================================
;   Data una FBF ritorna TRUE se questa è rappresentabile con una
;   clausola di Horn o congiunzioni di clausole di Horn; 
;   ritorna NIL in caso contrario.
;==============================================================================

(defun hornp (fbf)
    (if (check-fbf fbf)
        (check-horn-and
            (defold fbf)
        )
        (spawn-str 0)
    )
)
    
;==============================================================================

(defun check-horn-and (cnf)
    (if (check-opand cnf)
        (and 
            (check-horn-and (second cnf))
            (check-horn-and (third cnf))
        )
        (<= (check-horn-and-helper cnf) 1)
    )
)

;==============================================================================
    
(defun check-horn-and-helper (cnf)
    (cond 
        ;----------------------------------------------------------------------
        ((check-opor cnf)
            (+ (check-horn-and-helper (second cnf))
            (check-horn-and-helper (third cnf)))
        )
        ;----------------------------------------------------------------------
        ((check-negation cnf) 0)
        ;----------------------------------------------------------------------
        (T 1)
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                             FUNZIONE DI SUPPORTO
;==============================================================================

(defun defold (fbf)
    (replace-opand
        (algorithm-skolem
            (algorithm-negation 
                (algorithm-imply fbf)
            )
        )
    )
)

;==============================================================================
;                            RIMOZIONE IMPLICAZIONI
;==============================================================================

(defun algorithm-imply (fbf)
    (cond 
        ;----------------------------------------------------------------------
        ((check-imply fbf)
            (list 
                'or 
                (list 
                    'not 
                    (algorithm-imply (second fbf))
                ) 
                (algorithm-imply (third fbf))
            )
        )
        ;----------------------------------------------------------------------
        ((or 
                (check-exists fbf) 
                (check-forall fbf)
            )
            (list 
                (first fbf) 
                (second fbf) 
                (algorithm-imply (third fbf))
            )
        )
        ;----------------------------------------------------------------------
        ((check-complex fbf) 
            (check-complex-modifier 
                fbf 
                (lambda (x) 
                    (algorithm-imply x)
                )
            )
        )
        ;----------------------------------------------------------------------
        (T fbf)
    )
)

;==============================================================================
;                            RIMOZIONE NEGAZIONI
;==============================================================================

(defun algorithm-negation (fbf)
    (cond 
        ;----------------------------------------------------------------------
        ((check-negation fbf)
            (let ((f (second fbf)))
                (cond 
                    ;----------------------------------------------------------
                    ((check-negation f)
                        (algorithm-negation (second f))
                    )
                    ;----------------------------------------------------------
                    ((check-opand f)
                        (cons 
                            'or 
                            (mapcar 
                                (lambda (x) 
                                    (algorithm-negation 
                                        (list 'not x)
                                    )
                                ) 
                                (cdr f)
                            )
                        )
                    )
                    ;----------------------------------------------------------
                    ((check-opor f)
                        (cons 
                            'and 
                            (mapcar 
                                (lambda (x) 
                                    (algorithm-negation 
                                        (list 'not x)
                                    )
                                ) 
                                (cdr f)
                            )
                        )
                    )
                    ;----------------------------------------------------------
                    ((check-forall f)
                        (list 
                            'exist 
                            (second f) 
                            (algorithm-negation 
                                (list 
                                    'not 
                                    (third f)
                                )
                            )
                        )
                    )
                    ;----------------------------------------------------------
                    ((check-exists f)
                        (list 
                            'forall 
                            (second f) 
                            (algorithm-negation 
                                (list 
                                    'not 
                                    (third f)
                                )
                            )
                        )
                    )
                    ;----------------------------------------------------------
                    ((check-complex f)
                        (list 
                            'not 
                            (algorithm-negation f)
                        )
                    )
                    (T fbf)
                )
            )
        )
        ;----------------------------------------------------------------------
        ((check-complex fbf)
            (check-complex-modifier 
                fbf 
                (lambda (x) 
                    (algorithm-negation x)
                )
            )
        )
        ;----------------------------------------------------------------------
        (T fbf)
    )
)

;==============================================================================
;                            ALGORITMO DI SKOLEM
;==============================================================================
        
(defun algorithm-skolem (fbf &optional (type nil) (ex nil))
    (cond 
        ;----------------------------------------------------------------------
        ((check-exists fbf)
            (algorithm-skolem 
                (third fbf) 
                type 
                (acons 
                    (second fbf) 
                    (if (null type) 
                        (skolem-variable) 
                        (skolem-function type)
                    ) 
                    ex
                )
            )
        )
        ;----------------------------------------------------------------------
        ((check-forall fbf)
            (algorithm-skolem 
                (third fbf) 
                (cons 
                    (second fbf) 
                    type
                ) 
                ex
            )
        )
        ;----------------------------------------------------------------------
        ((variablep fbf)
            (let ((x (assoc fbf ex)))
                (if x 
                    (cdr x) 
                    fbf
                )
            )
        )
        ;----------------------------------------------------------------------
        ((constp fbf) fbf)
        ;----------------------------------------------------------------------
        ((check-complex fbf) 
            (check-complex-modifier 
                fbf 
                (lambda (x) 
                    (algorithm-skolem x type ex)
                )
            )
        )
        ;----------------------------------------------------------------------
        (T fbf)
    )
)

;==============================================================================
;                             RIDUZIONE AND/NOT
;==============================================================================
        
(defun simply-andor (fbf)
    (cond 
        ;----------------------------------------------------------------------
        ((check-opor fbf)
            (simply-andor-helper 
                fbf 
                'or
            )
        )
        ;----------------------------------------------------------------------
        ((check-opand fbf)
            (simply-andor-helper 
                fbf 
                'and
            )
        )
        ;----------------------------------------------------------------------
        ((check-negation fbf)
            (list 
                'not 
                (simply-andor 
                    (second fbf)
                )
            )
        )
        ;----------------------------------------------------------------------
        (T fbf)
        ;----------------------------------------------------------------------
    )
)

;==============================================================================

(defun simply-andor-helper (fbf op)
    (cond 
        ;----------------------------------------------------------------------
        ((eq 1 (length fbf)) 
            (eq op 'or)
        )
        ;----------------------------------------------------------------------
        ((eq 2 (length fbf)) 
            (simply-andor (second fbf))
        )
        ;----------------------------------------------------------------------
        ((eq 3 (length fbf))
            (list 
                op
                (simply-andor (second fbf))
                (simply-andor (third fbf))
            )
        )     
        ;----------------------------------------------------------------------
        ((> (length fbf) 3)
            (list 
                op 
                (simply-andor (second fbf))
                (simply-andor (append 
                    (list op (third fbf))
                    (cdddr fbf)))
            )
        )
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                            DISTRIBUZIONE AND/OR
;==============================================================================

(defun replace-opand (fbf)
    (replace-opand-helper 
        (simply-andor fbf)
    )
)

;==============================================================================

(defun replace-opand-helper (fbf)
    (cond 
        ;----------------------------------------------------------------------
        ((check-opor fbf)
            (let (
                    (p1 (replace-opand-helper (second fbf)))
                    (p2 (replace-opand-helper (third fbf)))
                )
                (cond 
                    ((check-opand p1)
                        (list 
                            'and 
                            (replace-opand-helper (list 'or (second p1) p2))
                            (replace-opand-helper (list 'or (third p1) p2))
                        )
                    )
                    ((check-opand p2)
                        (list 
                            'and 
                            (replace-opand-helper (list 'or p1 (second p2)))
                            (replace-opand-helper (list 'or p1 (third p2)))
                        )
                    )
                    (T (list 'or p1 p2))
                )
            )
        )
        ;----------------------------------------------------------------------
        ((check-opand fbf)        
            (list 
                'and
                (replace-opand-helper (second fbf))
                (replace-opand-helper (third fbf))
            )
        )
        ;----------------------------------------------------------------------
        ((check-negation fbf) 
            (list 
                'not 
                (replace-opand-helper (second fbf))
            )
        )
        ;----------------------------------------------------------------------
        (T fbf)
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                           SEMPLIFICAZIONE AND/OR
;==============================================================================

(defun create-formula (cnf) 
    (cond 
        ;----------------------------------------------------------------------
        ((check-opand cnf)
            (create-formula-helper 
                cnf 
                'and 
                'check-opand
            )
        )
        ;----------------------------------------------------------------------
        ((check-opor cnf)
            (create-formula-helper 
                cnf 
                'or 
                'check-opor
            )
        )
        ;----------------------------------------------------------------------
        ((check-negation cnf)
            (list 
                'not 
                (create-formula 
                    (second cnf)
                )
            )
        )
        ;----------------------------------------------------------------------
        (T cnf)
        ;----------------------------------------------------------------------
    )
)

;==============================================================================

(defun create-formula-helper (cnf op funp)
    (let (
            (p1 (create-formula (second cnf)))
            (p2 (create-formula (third cnf)))
        ) 
        (append 
            (list op)
            (if (funcall funp p1) 
                (cdr p1) 
                (list p1)
            )
            (if (funcall funp p2) 
                (cdr p2) 
                (list p2)
            )
        )
    )
)

;==============================================================================
;                              FUNZIONI DI CONTROLLO
;==============================================================================
       
(defun termp (term)
    (or 
        (constp term)
        (variablep term)
        (c-functionp term)
    )
)

;==============================================================================

(defun constp (c)
    (or 
        (null c) 
        (numberp c)
        (c-symbolp c)
    )
)

;==============================================================================
      
(defun c-symbolp (s)
    (and 
        (symbolp s)
        (not (operatorp s))
        (not (variablep s))
    )
)

;==============================================================================
       
(defun operatorp (o)
    (or 
        (eq o 'not)
        (eq o 'and)
        (eq o 'or)
        (eq o '==>)
        (eq o 'forall)
        (eq o 'exist)
    )
)

;==============================================================================
      
(defun c-functionp (f)
    (and 
        (listp f)
        (not (null f))
        (c-symbolp (car f))
        (every 'termp (cdr f))
    )
)

;==============================================================================
;                              FUNZIONI DI CONTROLLO
;==============================================================================
;   FBF ::= <predicato>
;        |  <negazione> 
;        |  <opand-fbf> 
;        |  <opor-fbf> 
;        |  <implicazione>
;        |  <universale>
;        |  <esistenziale>  
;==============================================================================
       
(defun check-fbf (fbf)
    (or 
        (check-predicate fbf)
        (check-negation fbf)
        (check-opand fbf)
        (check-opor fbf)
        (check-imply fbf)
        (check-forall fbf)
        (check-exists fbf)
    )
)

;==============================================================================
      
(defun check-predicate (predicate)
    (or 
        (c-symbolp predicate)
        (c-functionp predicate)
    )
)

;==============================================================================
      
(defun check-negation (negation)
    (and 
        (listp negation)
        (eq 2 (length negation))
        (eq 'not (car negation))
        (check-fbf (cadr negation))
    )
)

;==============================================================================
       
(defun check-opand (opand-fbf)
    (and 
        (listp opand-fbf)
        (not (null opand-fbf))
        (eq 'and (car opand-fbf))
        (every 'check-fbf (cdr opand-fbf))
    )
)

;==============================================================================

(defun check-opor (opor-fbf)
    (and 
        (listp opor-fbf)
        (not (null opor-fbf))
        (eq 'or (car opor-fbf))
        (every 'check-fbf (cdr opor-fbf))
    )
)

;==============================================================================

(defun check-imply (imply-fbf)
    (and 
        (listp imply-fbf)
        (eq 3 (length imply-fbf))
        (eq '==> (car imply-fbf))
        (check-fbf (cadr imply-fbf))
        (check-fbf (caddr imply-fbf))
    )
)

;==============================================================================

(defun check-forall (univ-fbf)
    (and 
        (listp univ-fbf)
        (eq 3 (length univ-fbf))
        (eq 'forall (car univ-fbf))
        (variablep (cadr univ-fbf))
        (check-fbf (caddr univ-fbf))
    )
)

;==============================================================================

(defun check-exists (exist-fbf)
    (and 
        (listp exist-fbf)
        (eq 3 (length exist-fbf))
        (eq 'exist (car exist-fbf))
        (variablep (cadr exist-fbf))
        (check-fbf (caddr exist-fbf))
    )
)

;==============================================================================
;                        CONTROLLO/MODIFICA COMPLESSI
;==============================================================================

(defun check-complex (ce)
    (and 
        (listp ce)
        (not (null ce))
    )
)

;==============================================================================
  
(defun check-complex-modifier (ce fun)
    (cons 
        (car ce) 
        (mapcar 
            fun 
            (cdr ce)
        )
    )
)

;==============================================================================
;                                   EOF
;==============================================================================